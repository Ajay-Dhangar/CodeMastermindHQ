---
id: css-bootcamp-day-1
title: CSS Bootcamp Day 1
sidebar_label: Day 1
date: 2023-05-11 16:49:29
sidebar_position: 1
---

```mdx-code-block
import HtmlWindow from '@site/src/components/HtmlWindow';
import CodeBlock from '@theme/CodeBlock';
import card from '@site/src/css/markdown.module.css'
```

> ***I can provide you with a comprehensive list of CSS questions ranging from basic to advanced concepts. These questions will help you prepare thoroughly for your CSS interview. Let's get started:***

### Basic CSS Questions:

1. What does CSS stand for, and what is its primary purpose?
2. How do you include CSS in an HTML document?
3. Explain the difference between inline, internal, and external CSS.
4. What are selectors in CSS?
5. How do you select an HTML element using its ID in CSS?
6. What is a class selector, and how is it different from an ID selector?
7. Describe the box model in CSS.
8. What are the four essential properties of the box model?
9. How do you add comments in CSS?
10. What is the importance of the "z-index" property in CSS?

### Intermediate CSS Questions:

11. Explain the concept of specificity in CSS.
12. What is the difference between padding and margin?
13. How do you center an element horizontally and vertically in CSS?
14. What is the "float" property, and how does it work?
15. Describe the purpose of the "position" property in CSS.
16. What are pseudo-classes in CSS, and provide examples.
17. Explain the CSS "display" property and its values.
18. What is the CSS "box-sizing" property, and why is it important?
19. How can you create a responsive design using CSS?
20. What are media queries, and how are they used in CSS?

### Advanced CSS Questions:

21. What is CSS specificity, and how is it calculated?
22. Describe the difference between "em" and "rem" units in CSS.
23. How do you create CSS animations and transitions?
24. Explain the concept of flexbox and its advantages.
25. What is the CSS Grid layout, and how does it work?
26. What is the purpose of the "transform" property in CSS?
27. How can you optimize CSS for better performance?
28. Explain the "::before" and "::after" pseudo-elements in CSS.
29. What are CSS preprocessors like SASS and LESS, and why are they used?
30. Describe the concept of "vendor prefixes" in CSS and provide examples.

### CSS Mastery Questions:

31. How do you implement custom fonts in CSS?
32. What is the "box-shadow" property, and how can you use it for effects?
33. Explain the concept of "CSS sprites" for image optimization.
34. How can you create a sticky header or footer in CSS?
35. What are CSS variables (custom properties), and how do they work?
36. Describe the "currentColor" keyword in CSS.
37. How can you achieve a multi-column layout in CSS?
38. What is the "filter" property, and how can it be used for image effects?
39. Explain the concept of "CSS-in-JS" and its advantages.
40. How can you implement dark mode in a website using CSS?
