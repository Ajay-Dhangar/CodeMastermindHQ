---
id: welcome-ai
title: Welcome To Artificial Intelligence (AI) Course
sidebar_position: 1
sidebar_label: Welcome To AI
---

## 1. Introduction to Artificial Intelligence
- **Artificial Intelligence (AI):** Simulation of human intelligence in machines.
- **AI Problems:** Categorized into various types such as search problems, game playing, pattern recognition.
- **AI Techniques:** Rule-based systems, machine learning, expert systems, neural networks.
- **State Space Search:** Defining problems in a state space and using search algorithms.
- **Problem Characteristics:** Understanding the complexity and nature of AI problems.
- **Production Systems:** Knowledge representation using a set of rules for decision-making.

## Search Techniques
- **Issues in Search Program Design:** Considerations for designing search programs.
- **Un-informed Search:** BFS, DFS.
- **Heuristic Search Techniques:** Generate-and-Test, Hill Climbing, Best-first search, A* Algorithm.
- **Other Techniques:** Problem reduction, AO* algorithm, Constraint satisfaction, Means-Ends analysis.

## 2. Knowledge Representation Using Rules
- **Procedural Vs Declarative Knowledge:** Understanding different types of knowledge.
- **Logic Programming:** Overview of LISP and PROLOG.
- **Forward Vs Backward Reasoning:** Approaches to reasoning.
- **Matching Techniques:** Including partial matching and RETE matching algorithm.

## 3. Symbolic Logic
- **Propositional Logic, First Order Predicate Logic:** Representing relationships.
- **Computable Functions and Predicates:** Expressing functions and predicates.
- **Unification & Resolution, Natural Deduction:** Techniques in symbolic logic.
- **Structured Representations of Knowledge:** Semantic Nets, Partitioned Semantic Nets, Frames, Conceptual dependency, Conceptual graphs, scripts.

## 4. Reasoning Under Uncertainty
- **Non-monotonic Reasoning:** Introduction and logics for non-monotonic reasoning.
- **Statistical Reasoning:** Bayes theorem, Certainty factors, Bayesian probabilistic inference, Bayesian networks, Dempster-Shafer theory.
- **Fuzzy Logic:** Crisp sets, Fuzzy sets, Fuzzy logic control, Fuzzy inferences & Fuzzy systems.

## Natural Language Processing
- **Steps in NLP:** Overview of the NLP process.
- **Syntactic Processing and Augmented Transition Nets:** Techniques in language processing.
- **Semantic Analysis:** Understanding meaning.
- **NLP Understanding Systems:** Systems for comprehending natural language.

## Planning
- **Components of a Planning System:** Understanding the elements of planning.
- **Goal Stack Planning, Non-linear Planning:** Techniques in planning.
- **Hierarchical Planning, Reactive Systems:** Approaches to planning.

## 5. Expert Systems
- **Overview of Expert Systems:** Architecture and types.
- **Knowledge Acquisition and Validation Techniques:** Building and validating knowledge.
- **Expert System Shells:** Tools for building expert systems.

## Outcomes
1. Understand AI problem characteristics, state space approach, and production system framework.
2. Learn optimal search strategies and the use of heuristics.
3. Learn relational, inferential, inheritable, and procedural knowledge and corresponding knowledge representation approaches.
4. Introduction to applying AI problem-solving approaches to natural language processing, planning, and expert systems.

## Textbooks
1. [Artificial Intelligence](/), Elaine Rich and Kevin Knight, Tata McGraw-Hill Publications.
2. [Introduction To Artificial Intelligence & Expert Systems](/), Patterson, PHI Publications.
3. [Artificial Intelligence: A Modern Approach](/), Russell, Stuart, and Norvig, Peter.

## Reference Books
1. [Artificial Intelligence](/), George F Luger, Pearson Education Publications.
2. [Artificial Intelligence: A Modern Approach](/), Russell and Norvig, Prentice Hall.
3. [Artificial Intelligence](/), Robert Schalkoff, McGraw-Hill Publications.
4. [Artificial Intelligence and Machine Learning](/), Vinod Chandra S.S., Anand Hareendran S.
