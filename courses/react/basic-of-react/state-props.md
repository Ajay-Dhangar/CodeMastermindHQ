---
id: state-props
title: Managing State and Props in Components
sidebar_position: 6
sidebar_label: Managing State & Props
tags: [React, state management, props, component state, React state, React props, passing data, managing state in React, React components, React beginners, React state management, React props management]
meta:
  - name: description
    content: Learn how to manage state and pass data using props in React components. Understand the concepts of state and props and explore best practices for effective state management in React applications.
  - name: keywords
    content: React, state management, props, component state, React state, React props, passing data, managing state in React, React components, React beginners, React state management, React props management
---


In React, components are the building blocks of user interfaces, and they can be dynamic by managing state and receiving data through props. Understanding how to manage state and props is crucial for building interactive and data-driven React applications. In this section, we will explore the concepts of state and props, and learn how to use them effectively in React components.

## 1. Introduction to State and Props

### 1.1 What is State?
State is a fundamental concept in React that represents the data a component maintains and can change over time. It allows components to be dynamic and responsive to user interactions. State is managed within the component and can be modified using the `setState()` method, triggering a re-render of the component to reflect the updated state.

### 1.2 What are Props?
Props (short for properties) are used to pass data from a parent component to a child component. They allow data to flow downward in the component tree and enable the reusability of components with varying data. Props are immutable and cannot be changed within the child component.

## 2. Managing State in React Components

### 2.1 Using `useState` Hook
The `useState` hook is a built-in hook in React that allows functional components to have state. We will learn how to declare state variables using `useState` and update them to reflect changes in the component's state.

```jsx title="Counter.jsx"
import React, { useState } from 'react';

const Counter = () => {
  const [count, setCount] = useState(0);

  const increment = () => {
    setCount(count + 1);
  };

  const decrement = () => {
    setCount(count - 1);
  };

  return (
    <div>
      <h2>Count: {count}</h2>
      <button onClick={increment}>Increment</button>
      <button onClick={decrement}>Decrement</button>
    </div>
  );
};

export default Counter;
```

### 2.2 Conditional Rendering based on State
We will explore how to conditionally render components or content based on the value of the state. This allows us to display different elements depending on the state of the application.

```jsx title="ToggleContent.jsx"
import React, { useState } from 'react';

const ToggleContent = () => {
  const [showContent, setShowContent] = useState(false);

  const toggleContent = () => {
    setShowContent(!showContent);
  };

  return (
    <div>
      <button onClick={toggleContent}>
        {showContent ? 'Hide Content' : 'Show Content'}
      </button>
      {showContent && <p>Content to be displayed when the button is clicked.</p>}
    </div>
  );
};

export default ToggleContent;
```

## 3. Passing Data through Props

### 3.1 Parent-to-Child Data Flow
We will understand how to pass data from a parent component to a child component using props. This enables communication between components and allows us to render dynamic content in the child component.

```jsx title="ParentComponent.jsx"
import React from 'react';

const ParentComponent = () => {
  const message = 'Hello from Parent Component!';

  return <ChildComponent message={message} />;
};

const ChildComponent = (props) => {
  return <h2>{props.message}</h2>;
};

export default ParentComponent;
```

### 3.2 Handling Props in Class Components
In class components, props are accessed using the `this.props` syntax. We will learn how to handle and use props within class components.

```jsx title="Greeting.jsx"
import React from 'react';

class Greeting extends React.Component {
  render() {
    return <h2>Hello, {this.props.name}!</h2>;
  }
}

export default Greeting;
```

## 4. Best Practices for Managing State and Props

### 4.1 Lifting State Up
We will explore the concept of "lifting state up," which involves moving the state to the nearest common ancestor of components that share the same data. This improves data consistency and simplifies state management.

### 4.2 Using PropTypes
PropTypes are a useful tool for validating the data types of props being passed to components. We will learn how to use PropTypes to ensure that the right data is passed to components.

```jsx title="PersonInfo.jsx"
import React from 'react';
import PropTypes from 'prop-types';

const PersonInfo = (props) => {
  return (
    <div>
      <h2>Name: {props.name}</h2>
      <p>Age: {props.age}</p>
    </div>
  );
};

PersonInfo.propTypes = {
  name: PropTypes.string.isRequired,
  age: PropTypes.number.isRequired,
};

export default PersonInfo;
```

## 5. Additional Resources and Links

- [React State and Lifecycle](https://reactjs.org/docs/state-and-lifecycle.html)
- [React Hooks: useState](https://reactjs.org/docs/hooks-state.html)
- [React PropTypes](https://reactjs.org/docs/typechecking-with-proptypes.html)

