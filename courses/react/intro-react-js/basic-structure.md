---
id: basic-structure
title: Understanding the Basic Structure of a React Application
sidebar_position: 4
sidebar_label: Basic Structure
---

```mdx-code-block
import BrowserWindow from '@site/src/components/BrowserWindow';
import CodeBlock from '@theme/CodeBlock';
import card from '@site/src/css/markdown.module.css'
```


In this topic, we'll explore the foundational structure of a React application. We'll cover the essential files, their purposes, and how they work together to create a functional React project. By the end of this section, you'll have a solid understanding of the fundamental building blocks required to kickstart your React journey.

<BrowserWindow>
    <body >
        <div>
          <h1>Hello, React!</h1>
          <p>Welcome to my React application.</p>
        </div>
    </body>
</BrowserWindow>

### 1. HTML File (index.html)

The `index.html` file serves as the entry point of our React application. It's the starting point from where the React components are rendered into the DOM. Here's a typical `index.html` file for a React project:

```html title="index.html"
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>React App</title>
</head>
<body>
  <div id="root"></div>
</body>
</html>
```

**Explanation:**
- The `<!DOCTYPE html>` declaration specifies the document type and version.
- The `<html>` element is the root element of the HTML document, and `lang="en"` sets the language to English.
- Inside the `<head>` section, we define the document's metadata, including the character set and viewport settings.
- The `<title>` element sets the title of the web page displayed in the browser's title bar or tab.
- The `<body>` element contains the content of the web page.
- The `<div id="root"></div>` acts as a placeholder where our React components will be rendered.

### 2. JavaScript File (index.js)

The `index.js` file is another critical piece of our React application. It's responsible for rendering the root React component into the DOM. Let's take a look at a basic `index.js` file:

```javascript title="index.js"
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

ReactDOM.render(<App />, document.getElementById('root'));
```

**Explanation:**
- The `import` statements bring in the required modules. Here, we import `React`, `ReactDOM`, and our main `App` component.
- `ReactDOM.render()` is a method provided by ReactDOM to render our top-level component (`App`) into the DOM.
- The `<App />` syntax is JSX, a syntax extension for JavaScript that allows us to write React components in a more concise manner.

### 3. JavaScript File (App.js)

The `App.js` file is the main component of our React application. It represents the root of our component tree. Let's see a simple `App.js` example:

```javascript title="App.js"
import React from 'react';

function App() {
  return (
    <div>
      <h1>Hello, React!</h1>
      <p>Welcome to my React application.</p>
    </div>
  );
}

export default App;
```

**Explanation:**
- We import the `React` module, which is necessary whenever we use JSX syntax.
- The `App` function is a functional component in React. It returns JSX, describing the component's UI.
- In this example, we're returning a `<div>` containing an `<h1>` heading and a `<p>` paragraph.


### Resources and Links:

1. React Documentation: [React Official Documentation](https://reactjs.org/docs/getting-started.html)
2. JSX Introduction: [JSX Introduction - React Documentation](https://reactjs.org/docs/introducing-jsx.html)
3. React Basics: [React Basics - React Documentation](https://reactjs.org/docs/hello-world.html)
4. React DOM: [ReactDOM - React Documentation](https://reactjs.org/docs/react-dom.html)
5. React Functional Components: [React Components - React Documentation](https://reactjs.org/docs/components-and-props.html)

These resources provide in-depth explanations, examples, and further exploration of React concepts. Keep practicing and experimenting with React to build a solid foundation for your React development journey.


This section aims to provide beginners with a clear understanding of the essential files and their roles in a React application. It's important to reinforce the importance of the HTML file as the entry point and the role of the `index.js` file in rendering the root component. Additionally, we introduce the `App.js` file as the main component where the rest of the component tree will be built. Remember to encourage learners to explore the provided resources to deepen their knowledge of React concepts. Happy coding!