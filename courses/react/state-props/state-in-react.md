---
id: state-in-react
title: Managing State in React Components
sidebar_position: 1
sidebar_label: State in React Components
tags: [React, state management, managing state, React components, state in React, setState, React state example, React state tutorial, React state management]
meta:
  - name: description
    content: Learn how to manage state in React components. Understand the concept of state, when to use it, and how to implement it with code examples.
  - name: keywords
    content: React, state management, managing state, React components, state in React, setState, React state example, React state tutorial, React state management
---

```mdx-code-block
import BrowserWindow from '@site/src/components/BrowserWindow';
import CodeBlock from '@theme/CodeBlock';
import card from '@site/src/css/markdown.module.css'
import Demo from './Demo';
```

In React, state is a way to manage and store data that can change over time within a component. It's used to create dynamic and interactive user interfaces. State allows components to update and re-render when data changes, ensuring that the UI remains in sync with the underlying data.

State management is a crucial aspect of React development. It allows components to hold and manage data that can change over time, leading to dynamic and interactive user interfaces. In this section, we'll explore how to manage state in React components, starting from scratch.

## Introduction to State

State is an object that belongs to a component. It represents the data that the component needs to render and function properly. When state changes, React automatically re-renders the component to reflect the updated data.

## Setting Up State

To use state in a class component, you need to initialize it in the constructor. For functional components, you can use the `useState` hook to manage state.

### Example: Class Component

In class components, state is managed differently. Use the `constructor` method to initialize state, and `this.setState` to update it.

```jsx title="Counter.jsx"
import React, { Component } from 'react';

class Counter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0
    };
  }

  render() {
    return (
      <div>
        <p>Count: {this.state.count}</p>
        <button onClick={() => this.setState({ count: this.state.count + 1 })}>
          Increment
        </button>
      </div>
    );
  }
}

export default Counter;
```

### Example: Functional Component with useState Hook

In React, the `useState` hook is used to add state to functional components. It takes an initial value as an argument and returns an array containing the current state and a function to update it.

```jsx title="Counter.jsx"
import React, { useState } from 'react';

function Counter() {
  const [count, setCount] = useState(0);

  return (
    <div>
      <p>Count: {count}</p>
      <button onClick={() => setCount(count + 1)}>Increment</button>
    </div>
  );
}

export default Counter;
```

### Live Preview

```jsx live
function Counter() {
  const [count, setCount] = useState(0);

  return (
    <div>
      <p>Count: {count}</p>
      <button onClick={() => setCount(count + 1)}>Increment</button>
    </div>
  );
}
```

### Managing Complex State
State doesn't have to be limited to primitive values. It can also hold objects or arrays. Let's create a simple shopping cart example:

```jsx live
function ShoppingCart() {
  const [cart, setCart] = useState([]);

  const addToCart = (item) => {
    setCart([...cart, item]);
  };

  return (
    <div>
      <h2>Shopping Cart</h2>
      <ul>
        {cart.map((item, index) => (
          <li key={index}>{item}</li>
        ))}
      </ul>
      <button onClick={() => addToCart('Product A')}>Add to Cart</button>
    </div>
  );
}
```

## Updating State

State should not be modified directly. Instead, you use the `setState` method (for class components) or the state updater function (for functional components) to update state. React will then merge the new state with the existing state.

## State and Functional Components

In functional components, you can have multiple state variables. Each call to `useState` creates a new state variable, allowing you to manage separate pieces of data independently.

## Now, Create simple React App

Let's create a simple example to understand how to manage state in React. We'll build a counter component where the user can increase or decrease the count.

### Step 1: Create a New React App

Make sure you have Node.js and npm installed. If not, download and install them from the official website. Then, open your terminal and run:

```bash
npx create-react-app state-management-demo
cd state-management-demo
```

### Step 2: Modify the Default Component

Open the `src/App.js` file and replace its contents with the following code:

```jsx
import React, { Component } from 'react';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0
    };
  }

  render() {
    return (
      <div className="App">
        <h1>Counter App</h1>
        <p>Count: {this.state.count}</p>
        <button onClick={this.increment}>Increment</button>
        <button onClick={this.decrement}>Decrement</button>
      </div>
    );
  }

  increment = () => {
    this.setState({ count: this.state.count + 1 });
  };

  decrement = () => {
    this.setState({ count: this.state.count - 1 });
  };
}

export default App;
```

### Explanation of the Code

1. We import React and Component from the 'react' package.
2. We define a class `App` that extends the `Component` class.
3. In the constructor, we set the initial state of the component with `count` set to 0.
4. In the `render` method, we display the current count value and two buttons.
5. The `increment` and `decrement` methods update the state when the corresponding buttons are clicked using the `setState` method.

### Running the App

To see the app in action, run the following command in the terminal:

```bash
npm start
```

Visit `http://localhost:3000` in your browser to interact with the counter app.

<BrowserWindow>
    <body >
        <div>
          <Demo />
        </div>
    </body>
</BrowserWindow>


## Resources and Links for Beginners

1. React Official Documentation - State and Lifecycle: [React State and Lifecycle](https://reactjs.org/docs/state-and-lifecycle.html)
2. FreeCodeCamp's React State and Lifecycle Explained: [Article](https://www.freecodecamp.org/news/react-state-and-lifecycle-explained/)
3. React State Management: The Beginner's Guide: [Article](https://www.robinwieruch.de/react-state-management)
