---
id: usestate-hook
title: Using the useState Hook for Managing Local State
sidebar_position: 3
sidebar_label: useState Hook
tags: [React, useState hook, managing local state, functional components, state management, dynamic updates, code examples, React hooks, beginner's guide]
meta:
  - name: description
    content: Learn how to use the useState hook in React for managing local state within functional components. Master state management and dynamic updates with code examples and explanations.
  - name: keywords
    content: React, useState hook, managing local state, functional components, state management, dynamic updates, code examples, React hooks, beginner's guide
---



```mdx-code-block
import BrowserWindow from '@site/src/components/BrowserWindow';
import CodeBlock from '@theme/CodeBlock';
import card from '@site/src/css/markdown.module.css'
import Demo from './Demo';
```

In React, state is used to manage data that can change over time and affect the rendering of components. The `useState` hook is a built-in React hook that allows functional components to manage local state.

## Introduction to the `useState` Hook

The `useState` hook is a function that returns a state variable and a function to update that state. It provides a way to add state to your functional components without needing to convert them into class components.

## 1: Import the Hook

In your component file, start by importing the `useState` hook from the React library.

```jsx
import React, { useState } from 'react';
```

## 2: Using `useState` in the Component

Let's say you want to create a simple counter component. Here's how you can use the `useState` hook to manage the count state:

```jsx live
function Counter() {
  // Declare a state variable named "count" and a function "setCount" to update it
  const [count, setCount] = useState(0);

  return (
    <div>
      <p>Count: {count}</p>
      <button onClick={() => setCount(count + 1)}>Increment</button>
      <button onClick={() => setCount(count - 1)}>Decrement</button>
    </div>
  );
}
```

In this example, `count` is the state variable, and `setCount` is the function to update it. We initialize `count` with a default value of `0`.

## 3: Updating the State

When the buttons are clicked, the `setCount` function is called with the updated value. React then re-renders the component with the new state, and you'll see the updated count.

## Setting Up the Environment

Before we dive into using the `useState` hook, ensure you have a React development environment set up. If you haven't already, you can create a new React application using Create React App by running the following command:

```bash
npx create-react-app useStateExample
```

```bash
cd useStateExample
```

## Basic Usage of `useState`

Let's start by creating a simple counter application using the `useState` hook.

**1. Create a new file named `Counter.js` in the `src` directory:**

```jsx title="Counter.js"
//  src/Counter.js
import React, { useState } from 'react';

function Counter() {
  const [count, setCount] = useState(0);

  const increment = () => {
    setCount(count + 1);
  };

  const decrement = () => {
    setCount(count - 1);
  };

  return (
    <div>
      <h1>Counter App</h1>
      <p>Count: {count}</p>
      <button onClick={increment}>Increment</button>
      <button onClick={decrement}>Decrement</button>
    </div>
  );
}

export default Counter;
```

**2. Import and use the `Counter` component in the `src/App.js` file:**

```jsx title="App.js"
// src/App.js
import React from 'react';
import Counter from './Counter';

function App() {
  return (
    <div className="App">
      <Counter />
    </div>
  );
}

export default App;
```

<BrowserWindow>
    <body >
        <div>
          <Demo />
        </div>
    </body>
</BrowserWindow>

## Explanation and Walkthrough

1. In the `Counter.js` component, we import the `useState` hook from React.

2. Inside the `Counter` functional component, we declare a state variable `count` and a function `setCount` to update it using the `useState` hook. The initial value of `count` is set to 0.

3. We define two functions, `increment` and `decrement`, that update the `count` state by increasing or decreasing its value.

4. Within the `return` statement, we render the current value of `count` and two buttons. Clicking the buttons calls the `increment` and `decrement` functions.

5. In the `App.js` file, we import and use the `Counter` component, which displays the counter and buttons.


## Additional Resources

1. React's official documentation on the [useState Hook](https://reactjs.org/docs/hooks-state.html)
2. Video tutorial: [React Hooks Tutorial for Beginners](https://www.youtube.com/watch?v=f687hBjwFcM)
3. Article: [A Complete Guide to useState](https://daveceddia.com/usestate-hook-examples/)

:::note
The `useState` hook is a powerful tool for managing local state in React functional components. It allows you to easily create and update state variables, which in turn controls how your components are rendered. By using the `useState` hook, you can keep your components simple and stateful, even if they're not class-based.

Remember to practice and experiment with different scenarios to solidify your understanding of using the `useState` hook effectively.
:::