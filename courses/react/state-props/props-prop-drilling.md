---
id: props-prop-drilling
title: Understanding Props and Prop Drilling
sidebar_position: 2
sidebar_label: Props and Prop Drilling
tags: [React, props, prop drilling, component communication, state management, beginner tutorial]
keywords: [React props, prop drilling explanation, passing props in React, prop drilling example, prop drilling in React, React component communication, state management in React, React tutorial for beginners]
---


```mdx-code-block
import BrowserWindow from '@site/src/components/BrowserWindow';
import CodeBlock from '@theme/CodeBlock';
import card from '@site/src/css/markdown.module.css'
import Test from './Test';
```

In React, **props** (short for properties) are a fundamental concept for passing data from a parent component to a child component. Props allow you to make your components reusable and dynamic by providing them with the necessary data to render. Props are immutable, meaning they cannot be modified by the child component that receives them.

## What are Props?

Props are essentially parameters that you pass to a component when you use it. They are read-only and cannot be modified by the component receiving them. Props are used to customize the behavior and appearance of a component based on the data passed to it.

## Passing and Using Props

Let's create a simple example to demonstrate how props work. We'll create a parent component called `ParentComponent` and a child component called `ChildComponent` to showcase the concept of props.

**1. Create a new file named `ParentComponent.js`:**

```jsx
import React from 'react';
import ChildComponent from './ChildComponent';

const ParentComponent = () => {
  const message = "Hello from Parent!";
  
  return (
    <div>
      <h1>Parent Component</h1>
      <ChildComponent message={message} />
    </div>
  );
};

export default ParentComponent;
```

**2. Create another file named `ChildComponent.js`:**

```jsx
import React from 'react';

const ChildComponent = (props) => {
  return (
    <div>
      <h2>Child Component</h2>
      <p>{props.message}</p>
    </div>
  );
};

export default ChildComponent;
```

<BrowserWindow>
    <body >
        <div>
          <h1>Parent Component</h1>
          <div>
            <h2>Child Component</h2>
            <p>Hello from Parent!</p>
          </div>
        </div>
    </body>
</BrowserWindow>

In this example, we have a `ParentComponent` that passes a `message` prop to the `ChildComponent`. The `ChildComponent` then receives and displays the message.


## Prop Drilling

Prop drilling occurs when you need to pass props through multiple levels of nested components. While it works, it can become cumbersome as your app grows, leading to components that aren't directly using the prop but merely passing it along. This is where **context** and **Redux** come into play for more advanced state management.

To mitigate excessive prop drilling, let's look at an example:

```jsx title="GrandparentComponent.js"
import React from 'react';
import ParentComponent from './ParentComponent';

function GrandparentComponent() {
  const message = "Hello from Grandparent!";
  return (
    <div>
      <ParentComponent message={message} />
    </div>
  );
}

export default GrandparentComponent;
```

```jsx title="ParentComponent.js"
import React from 'react';
import ChildComponent from './ChildComponent';

function ParentComponent(props) {
  return <ChildComponent message={props.message} />;
}

export default ParentComponent;
```

```jsx title="ChildComponent.js"
import React from 'react';

function ChildComponent(props) {
  return <div>{props.message}</div>;
}

export default ChildComponent;
```

<BrowserWindow>
    <body >
        <div>
          <Test />
        </div>
    </body>
</BrowserWindow>

Here, `message` prop is passed from `GrandparentComponent` to `ParentComponent` and then to `ChildComponent`. While this demonstrates prop drilling, it's not the most efficient approach.

:::tip
## Prop Drilling
Prop drilling occurs when you have a chain of nested components, and you need to pass props through intermediate components that don't directly use them. This can lead to a situation where components far down the hierarchy receive props they don't directly need. While prop drilling works, it can become complex and less efficient as your app grows.

### Using Context or Redux

To avoid excessive prop drilling, you can use tools like **Context API** or **Redux** for global state management. Context API allows you to pass data through the component tree without manually passing props at every level.
:::

## Resources and Links

1. [React Props documentation](https://reactjs.org/docs/components-and-props.html)
2. [React Context documentation](https://reactjs.org/docs/context.html)
3. [React Redux documentation](https://react-redux.js.org/introduction/quick-start)


:::note
Props are a crucial concept in React that allow components to communicate and share data. While prop drilling can be used, it's important to consider using tools like Context API or Redux for more complex applications to manage state more efficiently.
:::



