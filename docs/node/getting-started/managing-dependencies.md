---
id: managing-dependencies
sidebar_position: 4
title: Managing Dependencies in Node.js
sidebar_label: 🧑‍💻Managing Dependencies
---

In Node.js development, you'll often need to use external libraries or modules to enhance your applications. Managing these dependencies efficiently is crucial. Fortunately, Node.js provides a powerful package management system called **npm** (Node Package Manager) for this purpose.

## What is npm?

**npm** is a package manager that comes bundled with Node.js. It enables you to:

- Discover and install packages (libraries, modules) created by other developers.
- Keep track of dependencies in your project.
- Share your own packages with the Node.js community.

## Initializing a Node.js Project

Before you can manage dependencies with npm, you need to initialize your Node.js project. Open your terminal or command prompt and navigate to your project's directory. Then, run the following command:

```bash
npm init
```

This command will prompt you to answer questions about your project, such as its name, version, description, entry point, and more. Once you've answered these questions, npm will generate a `package.json` file that contains your project's configuration and metadata.

## Installing Dependencies

To install dependencies for your project, you can use the `npm install` command followed by the package name you want to install. For example, let's say you want to install the **Express.js** framework, a popular choice for building web applications in Node.js:

```bash
npm install express
```

This command will download the Express.js package and add it as a dependency to your project. It will also update your `package.json` file to include the dependency.

## Using `package.json`

The `package.json` file is central to managing your project's dependencies. It lists all the packages your project depends on and their versions. You can also define scripts for tasks like starting your application or running tests.

Here's an example of a `package.json` file:

```json
{
  "name": "my-node-app",
  "version": "1.0.0",
  "description": "A sample Node.js application",
  "main": "app.js",
  "dependencies": {
    "express": "^4.17.1"
  },
  "scripts": {
    "start": "node app.js"
  }
}
```

## Installing All Dependencies

If you have a `package.json` file, you can install all the listed dependencies by running:

```bash
npm install
```

This command will read your `package.json`, download and install all the listed packages and their dependencies.

## Saving Dependencies

By default, npm will save installed packages as dependencies in your `package.json` file. If you want to save a package as a development dependency (for testing, building, etc.), you can use the `--save-dev` or `-D` flag:

```bash
npm install package-name --save-dev
```

## Removing Dependencies

To remove a dependency, use the `npm uninstall` command:

```bash
npm uninstall package-name
```

This will uninstall the package and remove it from your `package.json` file.

Managing dependencies in Node.js is a vital skill. It ensures that your projects stay up-to-date and maintainable by allowing you to easily add, update, or remove external packages as needed.

Remember, as a beginner, using npm effectively will become second nature as you gain more experience in Node.js development. It's a fundamental skill that will serve you well throughout your journey in web development.