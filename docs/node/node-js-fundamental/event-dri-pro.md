---
id: event-driven-programming
sidebar_position: 4
title: Event-Driven Programming in Node.js
tags: [Node.js, Event-Driven Programming, Event Loop, EventEmitter, Asynchronous Programming, Real-Time Communication, Callback Functions, Observer Pattern, Scalable Applications, High-Performance Software, JavaScript Runtime, Non-Blocking I/O, Custom Events, Web Development, Beginner's Guide, Tutorial, Node.js Concepts, Node.js Fundamentals, Asynchronous Operations, Responsive Applications]
description: Learn event-driven programming in Node.js for efficient, responsive applications.
sidebar_label: 🧑‍💻Event-Driven Programming
---

```mdx-code-block
import BrowserWindow from '@site/src/components/BrowserWindow';
import CodeBlock from '@theme/CodeBlock';
import card from '@site/src/css/markdown.module.css'
```

Node.js is built on an event-driven architecture, which means that the execution of code is determined by events. Events can be anything from a user clicking a button to data arriving from a server or a file being read. Understanding event-driven programming is vital in Node.js as it enables you to build scalable and efficient applications.

### The Event Loop

At the heart of Node.js's event-driven architecture is the event loop. The event loop is a continuous process that listens for and responds to events. Here's how it works:

1. **Node.js starts** and begins executing the code sequentially.
2. When it encounters an asynchronous operation (e.g., reading a file or making an HTTP request), it **registers a callback function** and continues to the next operation.
3. While the asynchronous operation is being performed (e.g., waiting for a file to be read), Node.js doesn't block the rest of the code execution. Instead, it continues to process other tasks.
4. When the asynchronous operation is complete, **an event is emitted** to signal that it's done.
5. The event loop **calls the registered callback function** associated with that event.

This event loop allows Node.js to handle a large number of concurrent connections efficiently.

### EventEmitter

In Node.js, the `EventEmitter` class is at the core of event-driven programming. It provides an implementation of the observer pattern, allowing objects to subscribe to specific events and be notified when those events occur.

Let's look at an example using `EventEmitter`:

```javascript
const EventEmitter = require('events');

// Create a new EventEmitter instance
const myEmitter = new EventEmitter();

// Register an event listener for a custom event 'myEvent'
myEmitter.on('myEvent', (data) => {
  console.log(`Event 'myEvent' was triggered with data: ${data}`);
});

// Emit the 'myEvent' event with some data
myEmitter.emit('myEvent', 'Hello, World!');
```

In this example:
- We create a new `EventEmitter` instance.
- We register an event listener for the custom event `'myEvent'`. When this event is emitted, the provided callback function is executed.
- We emit the `'myEvent'` event with the data `'Hello, World!'`.

### Built-In Events in Node.js

Node.js provides various built-in modules that emit events. For example:

- The `http` module emits events related to HTTP requests and responses.
- The `fs` (file system) module emits events when working with files.
- The `stream` module emits events when working with streams of data.

Here's a simple example using the `http` module:

```js
const http = require('http');

// Create an HTTP server
const server = http.createServer((req, res) => {
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.end('Hello, Event-Driven World!\n');
});

// Listen on port 3000
server.listen(3000);

// Register an event listener for the 'listening' event
server.on('listening', () => {
  console.log('Server is listening on port 3000');
});
```

In this example, we create an HTTP server using the `http` module, and it emits a `'listening'` event when it starts listening on a port.

### Custom Events in Your Applications

Event-driven programming in Node.js allows you to create custom events in your applications, making it easy to handle complex logic asynchronously. You can build real-time applications, web servers, and more using these principles.

Understanding event-driven programming is fundamental in Node.js, and it enables you to harness the power of non-blocking I/O efficiently, making your applications fast and responsive.
