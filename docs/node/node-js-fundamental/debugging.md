---
id: debugging-node-app
sidebar_position: 7
title: Debugging Node.js Applications
tags: []
sidebar_label: 🧑‍💻Debugging Node Apps
---

```mdx-code-block
import BrowserWindow from '@site/src/components/BrowserWindow';
import CodeBlock from '@theme/CodeBlock';
import card from '@site/src/css/markdown.module.css'
```