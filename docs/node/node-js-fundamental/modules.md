---
id: modules-require
sidebar_position: 2
title: Node.js Modules and the require System
tags: [node.js modules, require system, JavaScript modules, exports, custom modules, core modules, Node.js require, module creation, Node.js Module Loading, Exporting Functions, Requiring Functions, Encapsulating Code, Organizing Code, Module Scope, Global Object in Node.js, Node.js Development, JavaScript Modules, Node.js Learning, Node.js for Beginners, Module Encapsulation, Exporting Variables, Core Node.js Modules, Using require in Node.js, Node.js Tutorial]
sidebar_label: 🧑‍💻Node.js Modules & require
description: Learn Node.js Modules & Require System. Master module creation, exports, and require statements.
---

> Learn about Node.js Modules and the require system in this comprehensive module. Understand how to encapsulate and organize your code with modules, whether they are built-in Node.js core modules or your custom modules. Discover the require system, which allows you to seamlessly load modules into your Node.js applications. Gain a firm grasp of exporting and requiring functions and data, and understand the scope of the global object in Node.js. Master this fundamental concept to build maintainable and organized Node.js applications.

***In Node.js, modules are a fundamental building block of your applications. Modules are a way to encapsulate and organize your code, making it more manageable and reusable. The `require` system in Node.js allows you to load modules into your applications. In this module, we will explore Node.js modules and the `require` system in detail.***

## Understanding Modules

### What are Modules?

In Node.js, a module is a separate file that contains reusable code. Each module is designed to perform a specific set of tasks or provide specific functionality. These modules can be your own code, built-in Node.js modules, or third-party modules.

### Creating Your Own Modules

To create a module, you simply write your code in a separate file. For example, let's create a module that calculates the area of a rectangle.

```javascript title="rectangle.js"
function calculateArea(width, height) {
  return width * height;
}

module.exports = calculateArea;
```

In this example, we define a function `calculateArea` and export it using `module.exports`.

## Using the `require` System

The `require` system is used to load modules into your Node.js applications. You can load built-in modules, modules from the Node.js standard library, or your own custom modules.

### Loading Built-In Modules

Node.js comes with a set of built-in core modules that you can use without installing anything. For example, to work with the file system, you can use the `fs` module.

```js
const fs = require('fs');
```

### Loading Custom Modules

To load your own custom modules, you specify the path to the module file. If the module is in the same directory as your script, you can use a relative path.

```js
const calculateArea = require('./rectangle.js');
```

### Core Modules vs. Custom Modules

Node.js distinguishes between core modules and custom modules. Core modules, like `fs` and `http`, are loaded using the module name. Custom modules are loaded using a file path.

## Exporting and Requiring Functions and Data

### Exporting Functions

In Node.js, you can export functions, objects, or variables from a module. For example, our `rectangle.js` module exports the `calculateArea` function.

```js title="rectangle.js"
function calculateArea(width, height) {
  return width * height;
}

module.exports = calculateArea;
```

### Requiring Functions

To use this exported function in another file, you require the module and call the function.

```js
const calculateArea = require('./rectangle.js');
const area = calculateArea(5, 10);
console.log('Area:', area);
```

## Node.js Modules and the Global Object

In Node.js, each module has its own scope, and variables or functions defined in one module are not directly accessible in another module. This is different from the browser's global scope.

The `global` object in Node.js is global only to the current module, not to the entire application. You can use it to set global variables for the current module.

## Conclusion

Understanding Node.js modules and the `require` system is crucial for building maintainable and organized applications. Modules help you encapsulate functionality and avoid naming conflicts. In the next module, we'll explore the Node Package Manager (NPM) for managing and sharing modules with the Node.js community.
