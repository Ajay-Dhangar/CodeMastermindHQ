---
id: fs-module
sidebar_position: 5
title: File System Operations (fs module)
tags: []
sidebar_label: 🧑‍💻FS Module
---


```mdx-code-block
import BrowserWindow from '@site/src/components/BrowserWindow';
import CodeBlock from '@theme/CodeBlock';
import card from '@site/src/css/markdown.module.css'
```