---
id: objects-arrays
title: Objects and Arrays
sidebar_position: 5
tags: [JavaScript objects, Working with objects, JavaScript arrays, Creating arrays, Accessing array elements, Managing objects and arrays]
description: Discover JavaScript objects and arrays. Explore how to work with objects, create arrays, and access elements within them
sidebar_label: 🧑‍💻Objects & Arrays
---

## Objects:

In JavaScript, objects are a fundamental data structure. They allow you to group related data and functions into a single entity, making it easier to work with complex data. Objects are defined using curly braces `{}` and can contain properties and methods.

**Example of Creating an Object:**

```javascript
let person = {
  name: "John",
  age: 30,
  address: {
    street: "123 Main St",
    city: "Exampleville",
  },
  sayHello: function() {
    console.log("Hello, my name is " + this.name + "!");
  },
};

console.log(person.name); // Accessing object property
person.sayHello(); // Calling object method
```

In this example, `person` is an object with properties like `name` and `age`, and it also contains a nested object `address` and a method `sayHello`. You can access object properties using dot notation.

## Arrays:

Arrays are used to store lists of data. In JavaScript, an array is an ordered list of values, which can include other arrays or objects.

**Example of Creating an Array:**

```javascript
let colors = ["red", "green", "blue"];
console.log(colors[0]); // Accessing the first element
colors.push("yellow"); // Adding an element to the end
console.log(colors.length); // Array length
```

In this example, `colors` is an array containing strings. You can access array elements by their index (starting from 0), and you can add new elements to the array using methods like `push`. The `length` property gives you the number of elements in the array.

**Iterating Through Arrays:**

You can loop through the elements of an array using `for` loops or other iteration methods like `forEach`.

**Example of Iterating Through an Array:**

```javascript
let fruits = ["apple", "banana", "cherry"];

for (let i = 0; i < fruits.length; i++) {
  console.log(fruits[i]);
}

// Using forEach
fruits.forEach(function(fruit) {
  console.log(fruit);
});
```

In this example, we use both a `for` loop and the `forEach` method to iterate through the `fruits` array and print each element.

Understanding objects and arrays is crucial as you start working with more complex data structures and as you delve deeper into Node.js development. They enable you to store and manipulate data in a structured way, making your code more organized and efficient.