---
id: paragraph-tags
title: Paragraph Tag
sidebar_label: Paragraph Tag
sidebar_position: 3
date: 2021-02-25 11:08:29
description: Welcome to HTML mastery tutorials!
---

In HTML, the `<p>` tag is your go-to tool for creating well-structured text. It allows you to define paragraphs, making your content easy to read and visually appealing.

### Usage: Defining a Paragraph in HTML

The `<p>` tag is the cornerstone of structuring text in HTML. It's used to format content into distinct paragraphs. Each paragraph enclosed within the `<p>` tags is automatically separated by empty lines, providing a clean visual break. Remember that every opening `<p>` tag should be matched with a closing `</p>` tag.

```html
<p>
  <!-- Your paragraph content goes here -->
</p>
```

### Attributes and Styling

While the `<p>` tag is simple in its essence, you can elevate its functionality by incorporating attributes. You can assign a class or an id to make it easier to apply CSS styles. Additionally, inline styles can be added using the style attribute.

```html
<p class="my-pera" style="color: blue;">
  This is a styled paragraph.
</p>
```

### Best Practices

It's good practice to use the `<p> `tag primarily for textual content, ensuring a logical and well-structured presentation. For layout control and structuring your document, HTML5 offers semantic tags like `<section>` and `<article>`. Furthermore, CSS techniques can be employed to enhance layout and presentation. We'll delve into these advanced concepts later in this tutorial.

<video controls width="100%">
  <source src="/video/paragraph.mp4" type="video/mp4" />
</video>

### Creating a Paragraph in HTML

To create a paragraph in HTML, simply wrap your content within `<p>` and `</p>` tags. Here's an example:

```html
<p>
  HTML paragraphs are a fundamental building block for well-structured text.
</p>
```

In this tutorial, we'll explore more about semantic tags, advanced HTML elements, and styling techniques to help you master the art of web development