---
id: line-break-tags
title: Line Break Tag
sidebar_label: Line Break Tag
sidebar_position: 4
date: 2021-02-25 11:08:29
description: Welcome to HTML mastery tutorials!
---

```mdx-code-block
import BrowserWindow from '@site/src/components/BrowserWindow';
import CodeBlock from '@theme/CodeBlock';
import card from '@site/src/css/markdown.module.css'
```

In HTML, the `<br>` tag serves as a simple yet crucial tool for controlling line breaks within text or paragraphs. Its primary function is to create a line break, enabling the content to flow onto the next line without the need for an additional paragraph.

### Syntax:

```html
<br>
```

The `<br>` tag, often referred to as an empty or self-closing tag, doesn't require a closing tag. It operates as a standalone element, ensuring a line break wherever it's placed.

### Usage:

Implementing the `<br>` tag is particularly beneficial when you want to maintain the original structure of certain textual content. It's commonly employed in various contexts, such as addresses, poems, song lyrics, or any content where preserving the visual layout or maintaining specific line breaks is essential for clarity and aesthetics.

For instance, in an address block, the `<br>` tag can be used to ensure the proper formatting of each line, as follows:

```html title="index.html"
<p>John Doe</p>
<p>1234 Main Street</p>
<p>City, State, ZIP</p>
```

The resulting output would display each line on a separate line:

<BrowserWindow url="http://127.0.0.1:5500/index.html">
    <p>John Doe</p>
    <p>1234 Main Street</p>
    <p>City, State, ZIP</p>
</BrowserWindow>

### Quick Demo Video:

Here's a quick video demonstrating the implementation of the `<br>` tag within an HTML document. The video illustrates how to use the tag effectively for managing line breaks and maintaining the desired text layout.

***video Coming Soon...***

