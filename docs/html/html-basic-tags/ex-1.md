---
id: skeletal-tags
title: Skeletal Tags
sidebar_label: Skeletal Tags
sidebar_position: 1
date: 2021-02-25 11:08:29
description: Welcome to HTML mastery tutorials!
---

In HTML, we have some fundamental tags that serve as the backbone of a web page's structure. These are often referred to as "skeletal tags."

### 1. `<html>` Tag: The Root of an HTML Page

```html
<html>
  <!-- Your content goes here -->
</html>
```

The `<html>` tag is the foundation of an HTML page. It encapsulates all the content on the webpage. Within an HTML document, you'll typically find two main sections: the `<head>...</head>` section and the `<body>...</body>` section.

### 2. `<head>` Tag: The Header Part of an HTML Page

```html
<head>
  <!-- Header Content, Meta Information, and Title -->
</head>
```

The `<head>` tag houses essential meta-information and the document's title. The title appears in the browser tab, while meta information is used for various purposes, including search engine optimization (SEO).

### 3. `<title>` Tag: The Title Part of an HTML Page

```html
<title>
  <!-- Title Name -->
</title>
```

The `<title>` tag defines the title of the document, which is displayed in the browser's title tab. It's crucial for identifying the webpage and enhancing user experience.

### 4. `<body>` Tag: The Body Part of an HTML Page

```html
<body>
  <!-- Body Content -->
</body>
```

The `<body>` tag encloses the primary content of the webpage. Anything within this tag is what users see when they visit your site. It contains text, images, links, and more.

Here's a visual representation of the skeletal tags and their role in defining the structure of a webpage:

```html title="index.html"
<html>
  <head>
    <!-- Meta information and Title -->
  </head>
  <body>
    <!-- Main content for users to see -->
  </body>
</html>
```

Understanding these skeletal tags is the first step in creating web pages. These tags provide the structure and foundation upon which you can build more complex and engaging websites.