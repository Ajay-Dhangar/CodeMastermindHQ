---
id: html-attributes
title: HTML Attributes
sidebar_label: HTML Attributes
sidebar_position: 8
date: 2021-02-25 11:08:29
description: Welcome to HTML mastery tutorials!
---

HTML attributes are crucial elements used to define characteristics or properties of HTML elements. They play a fundamental role in specifying how an element should behave or appear. Each attribute consists of two parts: the attribute name and its corresponding value.

1. **Name**: The attribute name identifies the specific property that you want to assign to an HTML element.

2. **Value**: The value part of an attribute sets the property's value for the element.

## Types of HTML Attributes

HTML attributes can be categorized into three main types, each serving a distinct purpose:

1. **Core Attributes**: These are fundamental attributes that can be applied to most HTML elements. Core attributes provide foundational functionality and styling options. 

2. **Internationalization Attributes**: Internationalization attributes assist in adapting web documents to different languages and regions. They are especially useful for creating multilingual websites.

3. **Generic Attributes**: Generic attributes offer additional information about an element without necessarily affecting its appearance or behavior. They are often used to store custom data specific to the page or application.

## Core Attributes

Among the core attributes, there are four main types:

### 1. ID Attribute

The "id" attribute is employed to assign a unique identifier to an HTML element. This identifier distinguishes one element from another, just as each individual has a unique identity. Unlike class attributes, multiple elements cannot share the same "id."

**Example:**

```html
<p id="html">This is an HTML tutorial</p>
<p id="python">This is a Python tutorial</p>
```

In this example, the "id" attribute is used to differentiate between two paragraphs by providing them with distinct values, "html" and "python." The "id" attribute is especially useful when you want to target a specific element with CSS or JavaScript.

### 2. Class Attribute

The "class" attribute is used to associate an HTML element with a particular class. It is typically utilized for styling or JavaScript manipulation. Unlike the "id" attribute, the "class" attribute is not unique, and multiple elements can share the same class.

**Example:**

```html
<p class="important">This is an important message.</p>
<p class="important">Another important message.</p>
```

In this example, both paragraphs share the same "important" class, allowing you to apply consistent styling or behavior to multiple elements. This attribute is commonly used in CSS for styling and in JavaScript for selecting elements with a specific class.

### 3. Title Attribute

The "title" attribute provides supplementary information about an element. When a user hovers their mouse pointer over an element, a tooltip is displayed, showing the information provided in the "title" attribute. This additional context can be particularly useful for user interface elements.

**Example:**

```html
<a href="https://example.com" title="Visit Example.com">Click here</a>
```

In this example, when a user hovers over the link, a tooltip with the text "Visit Example.com" is displayed. This can help provide additional information or clarify the purpose of the element.

### 4. Style Attribute

The "style" attribute allows for inline styling of HTML elements. It enables you to apply CSS properties directly to individual elements within the HTML code. This provides a way to customize the appearance of specific elements.

**Example:**

```html
<p style="color: red; font-size: 16px;">This text is red and larger.</p>
```

In this example, the "style" attribute is used to define the text color and font size for the paragraph. This inline styling overrides any external CSS styles and allows precise control over an element's appearance.

## Case Sensitivity

While HTML attributes are case-insensitive, it is recommended to use lowercase attribute names for better compatibility with stricter document types and best practices. For instance, "title" and "TITLE" are both acceptable, but using lowercase attributes is a common practice.

By understanding and utilizing HTML attributes, you can enhance the functionality and appearance of your web content and create well-structured, accessible, and visually appealing web pages. These attributes play a crucial role in both front-end development and web design, making them essential for anyone working with HTML.