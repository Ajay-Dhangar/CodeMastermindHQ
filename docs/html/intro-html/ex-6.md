---
id: html-tags
title: HTML Tags
sidebar_label: HTML Tags
sidebar_position: 6
date: 2021-02-25 11:08:29
description: Welcome to HTML mastery tutorials!
---


HTML tags are fundamental elements used to structure and format content on a webpage. They act as containers for content and other HTML tags, guiding web browsers on how to display and format that content.
HTML tags are used to structure and format content on a web page. They are enclosed in angle brackets (`<>` or `</>`) and often come in pairs with an opening and a closing tag.

## Commonly Used Tags in HTML
These are some of the most frequently used HTML tags, forming the backbone of web development.

### Document Structure Tags
1. `<!DOCTYPE html>`: Specifies the document type, ensuring proper rendering.
2. `<html>`: Encloses the entire HTML document.
3. `<head>`: Contains meta-information, links to scripts, and stylesheets.
4. `<body>`: Contains the visible content of the webpage.

### Metadata Tags
1. `<title>`: Sets the title displayed in the browser tab.
2. `<meta>`: Provides metadata such as character set, author, and viewport settings.
3. `<link>`: Links external resources like stylesheets.

### Text Formatting Tags
1. `<p>`: Represents a paragraph.
2. `<h1>`, `<h2>`, `<h3>`, `<h4>`, `<h5>`, `<h6>`: Define headings of different levels.
3. `<strong>`: Indicates strong emphasis, often displayed as bold text.
4. `<em>`: Represents emphasis, typically displayed as italic text.
5. `<br>`: Inserts a line break.
6. `<hr>`: Adds a horizontal rule.

### List Tags
1. `<ul>`: Creates an unordered list.
2. `<ol>`: Generates an ordered list.
3. `<li>`: Represents a list item.

### Hyperlink and Media Tags
1. `<a>`: Defines an anchor for creating hyperlinks.
2. `<img>`: Embeds images.
3. `<audio>`: Includes audio content.
4. `<video>`: Embeds video content.

### Form Tags
1. `<form>`: Creates a form for user input.
2. `<input>`: Adds input fields.
3. `<textarea>`: Provides a larger text input area.
4. `<button>`: Generates buttons.
5. `<select>`: Creates dropdown lists.
6. `<option>`: Defines options within `<select>` or `<datalist>`.

### Table Tags
1. `<table>`: Constructs tables.
2. `<tr>`: Represents table rows.
3. `<td>`: Specifies table data cells.
4. `<th>`: Indicates table header cells.
5. `<thead>`: Groups table headers.
6. `<tbody>`: Groups table body content.
7. `<tfoot>`: Groups table footer content.

### Semantic Tags
1. `<header>`: Marks the header section.
2. `<footer>`: Marks the footer section.
3. `<article>`: Defines an article.
4. `<section>`: Represents a section.
5. `<nav>`: Indicates navigation content.
6. `<aside>`: Represents sidebar content.

### Paired and Unpaired HTML Tags
HTML tags can be categorized into two types:

**1. Paired Tags (Container Tags)**

These tags come in pairs with an opening and a corresponding closing tag. Content goes between these tags.

Opening Tag: Starts with `<` and ends with `>`, for example, `<p>`.
Closing Tag: Starts with `<` and includes a forward slash `/` before the tag name, and ends with `>`, like `</p>`.

**Examples:**

- Paragraph: `<p>This is a paragraph.</p>`
- Headings: `<h1>This is a heading.</h1>`

**2. Unpaired Tags (Self-Closing Tags)**

These tags don't require a closing tag and encapsulate all the information within a single tag. Self-closing tags start with `<` and end with `/`, for example, `<img />`.

**Examples of Self-Closing Tags:**

- Line Break: `<br/>`
- Horizontal Rule: `<hr/>`
- Image: `<img src="image.jpg" alt="An example image"/>`

