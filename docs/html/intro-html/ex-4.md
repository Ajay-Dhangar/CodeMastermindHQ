---
id: html-execution
title: Your Journey to Creating Your First Website Begins Here!
sidebar_label: HTML Execution
sidebar_position: 4
date: 2021-02-25 11:08:29
description: Welcome to HTML mastery tutorials!
---

```mdx-code-block
import BrowserWindow from '@site/src/components/BrowserWindow';
import CodeBlock from '@theme/CodeBlock';
import card from '@site/src/css/markdown.module.css'
```

Welcome to our HTML journey! In this module, we'll guide you through the process of creating your first website. We'll start from the very beginning, assuming no prior knowledge, and help you take your first steps into the world of web development.

<video controls width="100%">
  <source src="/video/html-execution.mp4" type="video/mp4" />
</video>

## Creating a New File

Before we dive into coding, let's set up the foundation. In this module, you'll learn how to create a new HTML file where you'll write your code. We'll explore the basic tools and software you'll need to get started.

## Pasting or Writing the Code

Now it's time to write your first lines of HTML code. We'll cover the essential HTML elements and tags that form the building blocks of every web page. You'll learn how to structure your content, create headings, paragraphs, and more.

```html title="index.html"
<!DOCTYPE html>
<html>
<head>
    <title>My First Website</title>
</head>
<body>
    <h1>Welcome to My Website</h1>
    <p>This is a paragraph of text on our web page.</p>
</body>
</html>
```

<BrowserWindow url="http://127.0.0.1:5500/index.html">
    <h1>Welcome to My Website</h1>
    <p>This is a paragraph of text on our web page.</p>
</BrowserWindow>

## Going Live using the "Live Server" Extension

You've written your HTML code, and it's time to see it in action. In this module, you'll learn how to set up and use the "Live Server" extension in your code editor. 

Here's an example of how to install and use the extension:

1. Install the "Live Server" extension in your code editor.
2. Open your HTML file.
3. Right-click on your HTML file and select "Open with Live Server."
4. A new browser window will open, displaying your website in real-time.

## Your First Website is Live!

Congratulations, you've successfully created your first web page! In this module, we'll review your HTML code and make any necessary adjustments. You'll understand how to troubleshoot common issues and ensure your website looks and functions as intended.

## Live Preview Extension

In the final module, we'll delve deeper into the "Live Preview" extension. You'll discover how to take advantage of its advanced features, such as auto-refresh, responsive design testing, and more. This extension will become your trusted companion as you continue your web development journey.

Get ready to embark on your web development adventure! By the end of this tutorial series, you'll have the skills and knowledge to create and publish your own websites. Whether you're pursuing a career in web development or just exploring a new hobby, this is your starting point. Let's get started!