---
id: html-comments
title: HTML Comments
sidebar_label: HTML Comments
sidebar_position: 9
date: 2021-02-25 11:08:29
description: Welcome to HTML mastery tutorials!
---

HTML comments serve as annotations within your code, providing helpful notes for developers and maintaining code readability. The crucial aspect of comments is that they don't appear on the actual webpage; web browsers ignore them. This makes comments a valuable tool for explaining code, making it easier for you and other developers to understand and maintain the codebase.

## Key Points About HTML Comments

1. **Web Browser Ignorance**: HTML comments are completely ignored by web browsers. They have no impact on the visual appearance or functionality of a webpage.

2. **Readability and Documentation**: Comments are invaluable for improving code readability and acting as a form of documentation. They help you explain the purpose of specific sections or elements in your code.

3. **Comment Notation**: HTML comments are denoted by `<!--` at the beginning and `-->` at the end, with your comment content in between.

4. **Shortcut Key**: A common shortcut key to comment or uncomment code is `Ctrl + /` (or `Cmd + /` on Mac).

5. **Single-Line and Multi-Line Comments**: HTML supports both single-line and multi-line comments to cater to different use cases.

## Types of Comments in HTML

### Single-line Comments

Single-line comments are concise annotations that fit within a single line. They are perfect for short explanations or reminders within your code.

Example:
```html
<!-- This is a single-line comment -->
```

### Multi-line Comments

Multi-line comments span across multiple lines, allowing for more extensive explanations or temporarily disabling entire blocks of code.

Example:
```html
<!--
  This is a multi-line comment.
  It can span multiple lines and is ideal for detailed explanations.
-->
```

In the following video, you can observe how the content inside a comment is not rendered on the webpage, underlining the fact that comments remain hidden from the end-users.

<video controls width="100%">
  <source src="/video/comments.mp4" type="video/mp4" />
</video>

With the ability to add comments in your HTML code, you can significantly enhance your coding process. Whether you're working on personal projects or collaborating with other developers, clear and well-documented code is a valuable asset. HTML comments are a simple yet powerful feature to achieve this goal.