---
sidebar_position: 2
id: team
title: Team
date: 2022-05-08 16:10:21
tags: [CodeMasterMindHQ, Support, Team]
---

import { ActiveTeamRow } from '@site/src/components/TeamProfileCards';

## Active Team {#active-team}

At CodeMastermindHQ, we are proud to have a dedicated and passionate team that works tirelessly to bring you the best web development resources and support. Get to know the people behind our project: 

Current members of the CMHQ team are listed in alphabetical order below.

<ActiveTeamRow />

:::info
Ajay Dhangar is the visionary behind CodeMastermindHQ. With a passion for web development and a commitment to providing valuable resources to the community, he started this project to help developers at all levels.
:::

<!-- ## Honorary Alumni {#honorary-alumni}

CMHQ would never be what it is today without the huge contributions from these folks who have moved on to bigger and greater things.

<HonoraryAlumniTeamRow />

## Student Fellows {#student-fellows}

A handful of students have also worked on CMHQ, contributing amazing features such as plugin options validation, migration tooling, and a Bootstrap theme.

<StudentFellowsTeamRow />

## Acknowledgements {#acknowledgements}

CMHQ was originally created by Ajay Dhangar. Today, CMHQ has a few open source contributors. We’d like to recognize a few people who have made significant contributions to CMHQ and its CMHQ in the past and have helped maintain them over the years:
 -->
