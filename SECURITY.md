# Security Policy

## Supported Versions

We take security seriously and aim to provide security updates for the following versions of CodeMastermindHQ:

| Version | Supported          |
| ------- | ------------------ |
| Latest  | :white_check_mark: |
| < Latest| :x:                |

It's recommended to always use the latest version of the project to ensure you have the latest security enhancements.

## Reporting a Vulnerability

If you discover a security vulnerability within CodeMastermindHQ, please send an email to codemastermindhq@gmail.com. We take all security vulnerabilities seriously and will respond promptly to your disclosure.

In your email, please include the following details:

- A brief description of the vulnerability
- Steps to reproduce the vulnerability
- Expected vs. actual behavior
- Any potential impact of the vulnerability

We are committed to addressing security vulnerabilities in a timely manner and will keep you informed of the progress of your report. Please note that we may need some time to investigate and address the issue.

Thank you for helping us keep CodeMastermindHQ secure.
